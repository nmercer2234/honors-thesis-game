+++++ DeviantArt Stock +++++
http://seductive-stock.deviantart.com/
	http://www.deviantart.com/art/Cardinal-2-112537861  // cardinal l1

http://riktorsashen.deviantart.com/
	http://www.deviantart.com/art/Butterfly-61721835  // monarch butterfly l1
	http://riktorsashen.deviantart.com/art/Owls-119003329  // barred owl l1

http://kelbellestock.deviantart.com/
	http://www.deviantart.com/art/Squirrel-I-86543998 //squirrel l1
	http://fav.me/d1fixkh // duck l1
	http://fav.me/d1e8t0q // rabbit l1
	http://fav.me/d1auyhx  // "Tashmoo Path" used for background l1

+++++ Wikimedia Commons +++++

License: 2.0 Generic
http://commons.wikimedia.org/wiki/File:England_-_English_Summer_Woods_%287183006498%29.jpg // a background l2
http://commons.wikimedia.org/wiki/File:Striped_skunk_Freddy.jpg // skunk l2
http://commons.wikimedia.org/wiki/File:Spring_Fields_at_UCSC.jpg // a background l4
commons.wikimedia.org/wiki/File:Lake_in_Lower_Central_Park,_July_2008.jpg // bg l5
http://commons.wikimedia.org/wiki/Category:Grus_americana#mediaviewer/File:Whooping_Cranes_in_marsh_in_Texas.jpg // crane 3/5
commons.wikimedia.org/wiki/Category:Tamias#mediaviewer/File:Tamias_quadrimaculatus_IMG_6098.jpg // chipmunk 3
http://commons.wikimedia.org/wiki/Corvus_%28genus%29#mediaviewer/File:Corvus_albicollis_flight.jpg // crow 4
http://commons.wikimedia.org/wiki/Tyto_alba#mediaviewer/File:Barn_Owl_South_Acre_2.jpg // owl 4
http://commons.wikimedia.org/wiki/Category:Apis_%28insect%29#mediaviewer/File:Honey_Bees_in_Willow_Trees_%288345531686%29.jpg // bee 4
http://commons.wikimedia.org/wiki/Salmo_salar#mediaviewer/File:Jumping_salmon_1.jpg // salmon 5
http://commons.wikimedia.org/wiki/Meleagris_gallopavo#mediaviewer/File:Turkeys_running_%28cropped%29.jpg // turkey 5
http://commons.wikimedia.org/wiki/Category:Tabby_cats#mediaviewer/File:Fat_tabby_cat_drinking_water_from_a_pond-Hisashi-01.jpg // cat 5

License: Share-Alike 3.0 Unported
http://commons.wikimedia.org/wiki/File:Erithacus_rubecula_with_cocked_head.jpg // robin l2
http://commons.wikimedia.org/wiki/File:Mazama_americana.jpg // deer l2
http://commons.wikimedia.org/wiki/File:Blue_Jay_RWD4.jpg // blue jay l2
http://commons.wikimedia.org/wiki/File:Conejo_astronomo1.jpg // rabbit l2
http://commons.wikimedia.org/wiki/File:Bia%C5%82owieski_park_narodowy_05.jpg // a background l3
http://commons.wikimedia.org/wiki/Turtle#mediaviewer/File:Turtles_in_Atocha_garden_%28Madrid%29_01.jpg // turtle 5
http://commons.wikimedia.org/wiki/Anatidae#mediaviewer/File:Anas_falcata.JPG // ducks 3/5
http://commons.wikimedia.org/wiki/Trochilidae#mediaviewer/File:Colibr%C3%AD_Cola_de_Oro_%28Golden-tailed_Sapphire_Hummingbird%29_Bigger_File.jpg // hummingbird 5
http://commons.wikimedia.org/wiki/Sciuridae#mediaviewer/File:A_Sciuridae_in_Taipei_2.jpg // squirrel 3
http://commons.wikimedia.org/wiki/Procyon_lotor#mediaviewer/File:Mm_am_See.jpg // raccoon 3
http://commons.wikimedia.org/wiki/Frog#mediaviewer/File:Micrixalus.jpg // frog 3
http://commons.wikimedia.org/wiki/Cattle#mediaviewer/File:Glanrind_1.jpg // cow 4
http://commons.wikimedia.org/wiki/File:Coyote2008.jpg // coyote 5
http://commons.wikimedia.org/wiki/Cervus_elaphus#mediaviewer/File:Dortmund-Zoo-IMG_5481-a.jpg // deer 5
http://commons.wikimedia.org/wiki/Category:Chelydra_serpentina#mediaviewer/File:Adirondacks_-_Snapping_Turtle_-_7.JPG // turtle 5
http://commons.wikimedia.org/wiki/File:Rosa_Westerland_-_Giverny01.jpg // victory screen


License: Share-Alike 2.5 Generic
http://commons.wikimedia.org/wiki/File:Red_fox_norway_1.JPG // fox l2
http://commons.wikimedia.org/wiki/File:Dyoungi.jpg // bat l2
http://commons.wikimedia.org/wiki/Butterfly#mediaviewer/File:Argynnis_adippe_1_Richard_Bartz.jpg // butterfly 4
http://commons.wikimedia.org/wiki/Category:Didelphis_virginiana#mediaviewer/File:Opossum_2.jpg // possum 3

License: Public Domain
http://commons.wikimedia.org/wiki/Branta_canadensis#mediaviewer/File:Goose-flying.jpg // goose 4
http://commons.wikimedia.org/wiki/File:Seychelles_Fruit_Bat_-_Pteropus_seychellensis.jpg // bat 4