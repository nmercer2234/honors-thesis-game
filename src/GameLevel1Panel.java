import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.Timer;


public class GameLevel1Panel extends JPanel implements MouseListener{
	private ActionListener a = this.new MenuListener();
	private static CardLayout cards;
	private AllPanels pane;
	
	// Array containing the items hidden in the image
	private String[] listwords =  { "", "cardinal", "squirrel", "butterfly", "duck", 
			"owl", "", "rabbit" };
	// Other arrays for boxCheck
	private Point[] points = {new Point(0,0), new Point(505,431), new Point(699,300),
			new Point(210,125), new Point(420,427), new Point(201,216), 
			new Point(0,0), new Point(41,167)};
	private int[] x_size = { 0, 50, 70, 50, 50, 50, 0, 150 };
	private int[] y_size = { 0, 50, 70, 50, 50, 50, 0, 100 };
	
	/* Declare images */
	private Image[] bgin = new Image[9];
	private Image bino;
	private Image binobutton;
	private Image binopressed;
	private Image bgout;
	private Image checkmark;
	private Image watch;
	private Image pause;
	private Image play;
	private Image pausebg;
		
	private int bgx = 0; // bgx and bgy contain the coordinates
	private int bgy = 0; // of the current zoomed in image
	private boolean isZoomedIn = false;
	private boolean isBinoPressed = false;  // true when binocular button is being pressed
	private boolean[] allItemsFound = new boolean[9];
	private int totalFound = 0;
	private boolean showCheck = false;
	private int checkDuration = 0; // a counter used in paint function
	private int chx, chy; // coordinates of the on-screen check mark
	public static boolean hasBeenClicked = false; // true if user has clicked screen once
	
	private Timer t = new Timer(375, a); // timer that controls in game clock
	private boolean isPaused = false; // true if user pauses game
	private String time = "10:00"; // display: clock starts at 10am
	private int tstart = 10; // the 10 part of 10:00
	private int tend = 0; // the 00 part of 10:00
	private String bg_type = "DAY"; // used to switch between the three background
									// types: day, sunset, night
	
	

	public GameLevel1Panel(AllPanels p, CardLayout c){
		pane = p;
		cards = c;

		setLayout(null);
		addMouseListener(this);
		declareImages();
		
		for(int i = 0; i<9; i++){
			allItemsFound[i] = false;
		}
		// These are not going to be effected, so set them to true
		allItemsFound[0] = true;
		allItemsFound[6] = true;
		allItemsFound[8] = true;

		repaint();
	}
	
	/**
	 *  Resets the contents of the game to their initial states.
	 *  Used when game ends in case user wants to replay this level.
	 */
	public void resetLevel(){
		// Stop the timer and reset boolean that controls timer
		t.stop();
		hasBeenClicked = false;
		// Reset whether items have been found
		for(int i = 0; i<9; i++){
			allItemsFound[i] = false;
		}
		allItemsFound[0] = true;
		allItemsFound[6] = true;
		allItemsFound[8] = true;
		
		//Reset list words
		String[] replis = { "", "cardinal", "squirrel", "butterfly", "duck", 
				"owl", "", "rabbit" };
		for(int i = 0; i < replis.length; i++){
			listwords[i] = replis[i];
		}
		// Reset total found
		totalFound = 0;
		// Reset time variables
		time = "10:00";
		tstart = 10;
		tend = 0;
		// Reset background variables
		bg_type = "DAY";
		changeToDay();
		isZoomedIn = false;
		bgx = 0;
		bgy = 0;
		showCheck = false;
		System.out.println("Level reset!");
		
	}
	
	/**
	 * Called by: constructor, mouseClicked, mousePressed, mouseReleased, actionPerformed
	 */
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;
		
		if(!bg_type.equals("GAMEOVER")){
			// Changes the backgrounds at certain in-game times
			if(bg_type.equals("SUNSET")) {
				changeToSunset();
				bg_type = "We're good.";
			}
			if(bg_type.equals("NIGHT")){
				changeToNight();
				bg_type = "We're better.";
			}
			
			// Draws the background, either zoomed out or the current image
			if(!isZoomedIn)g2.drawImage(bgout, 1, 0, null);
			else g2.drawImage(currentImage(), bgx, bgy, null);
			
			// Determines if binocular button is being pressed and draws
			// correct image for button
			if(!isBinoPressed) bino = binobutton;
			else bino = binopressed;
			g2.drawImage(bino, 0, 440, null);
			
			// When an item is found, a check mark will display briefly
			// and the list's text will be updated
			if(showCheck) {
				if(checkDuration <= 5){
					g2.drawImage(checkmark, chx, chy, null);
					if(checkDuration == 0) updateText();
					checkDuration++;
				}
				else {  // Reset involved variables
					showCheck = false;
					checkDuration = 0;
				}
			}
			
			// Draws the watch timer
			g2.drawImage(watch, 650, 440, null);
			g2.setFont(new Font("Arial", Font.BOLD, 20));
			g2.setColor(new Color(87, 191, 76));
			g2.drawString(time, 690, 520);
			
			// Draws the pause screen or pause button
			if(isPaused) {
				g2.drawImage(pausebg, 0, 0, null);
				g2.drawImage(play, 750, -3, null);
				String found = "You have found " + totalFound + " out of 6!";
				g2.setColor(Color.BLACK);
				g2.drawString(found,  260, 400);
			}
			else g2.drawImage(pause, 750, -3, null); 
			
			revalidate(); // Not sure if this is still necessary?
		}
	}
	/**
	 * Initializes all of the involved images.
	 * Called in: constructor
	 */
	public void declareImages(){
		try {
			
			binobutton = ImageIO.read(new File("binobutton.png"));
			binopressed = ImageIO.read(new File("binobuttonpressed.png"));
			bgout = ImageIO.read(new File("bg1zoomedout.png"));
			bgin[0] = ImageIO.read(new File("bg1topleft.png"));
			bgin[1] = ImageIO.read(new File("bg1topmiddle.png"));
			bgin[2] = ImageIO.read(new File("bg1topright.png"));
			bgin[3] = ImageIO.read(new File("bg1middleleft.png"));
			bgin[4] = ImageIO.read(new File("bg1middlemiddle.png"));
			bgin[5] = ImageIO.read(new File("bgmiddleright.png"));
			bgin[6] = ImageIO.read(new File("bg1bottomleft.png"));
			bgin[7] = ImageIO.read(new File("bg1bottommiddle.png"));
			bgin[8] = ImageIO.read(new File("bg1bottomright.png"));
			checkmark = ImageIO.read(new File("check.png")); 
			watch = ImageIO.read(new File("clock.png"));
			pause = ImageIO.read(new File("pause.png"));
			play = ImageIO.read(new File("play.png"));
			pausebg = ImageIO.read(new File("pausebg.png"));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Changes the background images to their day variety.
	 * Called by: resetLevel
	 */
	public void changeToDay(){
		try{
		bgout = ImageIO.read(new File("bg1zoomedout.png"));
		bgin[0] = ImageIO.read(new File("bg1topleft.png"));
		bgin[1] = ImageIO.read(new File("bg1topmiddle.png"));
		bgin[2] = ImageIO.read(new File("bg1topright.png"));
		bgin[3] = ImageIO.read(new File("bg1middleleft.png"));
		bgin[4] = ImageIO.read(new File("bg1middlemiddle.png"));
		bgin[5] = ImageIO.read(new File("bgmiddleright.png"));
		bgin[6] = ImageIO.read(new File("bg1bottomleft.png"));
		bgin[7] = ImageIO.read(new File("bg1bottommiddle.png"));
		bgin[8] = ImageIO.read(new File("bg1bottomright.png"));
		} catch(IOException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Changes the background images to their sunset variety.
	 * Called by: paint
	 */
	public void changeToSunset(){
		try{
		bgout = ImageIO.read(new File("bg1zoomedout-sunset.png"));
		bgin[0] = ImageIO.read(new File("bg1topleft-sunset.png"));
		bgin[1] = ImageIO.read(new File("bg1topmiddle-sunset.png"));
		bgin[2] = ImageIO.read(new File("bg1topright-sunset.png"));
		bgin[3] = ImageIO.read(new File("bg1middleleft-sunset.png"));
		bgin[4] = ImageIO.read(new File("bg1middlemiddle-sunset.png"));
		bgin[5] = ImageIO.read(new File("bgmiddleright-sunset.png"));
		bgin[6] = ImageIO.read(new File("bg1bottomleft-sunset.png"));
		bgin[7] = ImageIO.read(new File("bg1bottommiddle-sunset.png"));
		bgin[8] = ImageIO.read(new File("bg1bottomright-sunset.png"));
		} catch(IOException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Changes the background images to their night variety.
	 * Called by: paint
	 */
	public void changeToNight(){
		try{
		bgout = ImageIO.read(new File("bg1zoomedout-night.png"));
		bgin[0] = ImageIO.read(new File("bg1topleft-night.png"));
		bgin[1] = ImageIO.read(new File("bg1topmiddle-night.png"));
		bgin[2] = ImageIO.read(new File("bg1topright-night.png"));
		bgin[3] = ImageIO.read(new File("bg1middleleft-night.png"));
		bgin[4] = ImageIO.read(new File("bg1middlemiddle-night.png"));
		bgin[5] = ImageIO.read(new File("bgmiddleright-night.png"));
		bgin[6] = ImageIO.read(new File("bg1bottomleft-night.png"));
		bgin[7] = ImageIO.read(new File("bg1bottommiddle-night.png"));
		bgin[8] = ImageIO.read(new File("bg1bottomright-night.png"));
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	public void mouseClicked(MouseEvent arg0) {
		if(!hasBeenClicked) {		// If mouse has been clicked once, start the timer.
			hasBeenClicked = true;
			t.start();
			VictoryPanel.displayImage = 0;
		}
		
		int x = arg0.getX();
		int y = arg0.getY();

		if(x > 750 && y > 0 && y < 50) { // If the pause/play button was clicked
			isPaused = !isPaused;
			repaint();
		}
	
		if(!isPaused){
			// If binocular button was clicked
			if(x > 0 && x < 110 && y < 550 && y > 450) isZoomedIn = !isZoomedIn;

			if(isZoomedIn){
				// See if an arrow was pressed to change images and update bgx/bgy
				if(y < 40 && x < 785) bgy -= 1;
				else if(x > 740 && y > 0) bgx += 1;
				else if(y > 525 && x > 250 && x < 750) bgy += 1;	
				else if(x < 40 && y > 200 && y < 325) bgx -= 1;	
				//else System.out.println("No proper area clicked");
				
				//These print the coordinates clicked (for debugging)
				System.out.println(x + " " + y + "bgy: "+ bgy);
				System.out.println(x + " " + y + "bgx: "+ bgx);
			}
			boxCheck(x,y); // Check if any items were clicked
			completionCheck();  // Check if all items have been found
			repaint();
		}
	}

	public void mouseEntered(MouseEvent arg0) {
		// Not used currently
	}

	public void mouseExited(MouseEvent arg0) {
		// Not used currently
	}

	public void mousePressed(MouseEvent arg0) {
		// Changes the binocular button to its pressed image if it is being pressed
		if(!isPaused){
			int x = arg0.getX();
			int y = arg0.getY();
			if(x > 0 && x < 110 && y < 550 && y > 450){
				isBinoPressed = true;
				repaint();
			}
		}
	}

	public void mouseReleased(MouseEvent arg0) {
		// Changes the binocular button to its not-pressed image
		if(isBinoPressed) {
			isBinoPressed = false;
			repaint();
		}
	}

	/** Checks bgx and bgy coordinates to see what the current
	 * background image should be.
	 * 
	 * @return Current background image to be painted
	 */
	public Image currentImage(){
		// Make sure that the coordinates aren't out of bounds
		if(bgx < 0) bgx = 0;
		if(bgy < 0) bgy = 0;
		if(bgx > 2) bgx = 2;
		if(bgy > 2) bgy = 2;
		
		// Pick which to return
		if(bgx == 0 && bgy == 0) return bgin[0];
		else if(bgx == 1 && bgy == 0) return bgin[1];
		else if(bgx == 2 && bgy == 0) return bgin[2];
		else if(bgx == 0 && bgy == 1) return bgin[3];
		else if(bgx == 1 && bgy == 1) return bgin[4];
		else if(bgx == 2 && bgy == 1) return bgin[5];
		else if(bgx == 0 && bgy == 2) return bgin[6];
		else if(bgx == 1 && bgy == 2) return bgin[7];
		else if(bgx == 2 && bgy == 2) return bgin[8];
		else System.err.println("Trying to find bg that doesn't exist."); return null;
		
	}
	
	public void boxCheck(int x, int y){
		Rectangle bound;

		for(int i = 0; i < 8; i++){
			if(currentImage() == bgin[i]){
				bound = new Rectangle(points[i]);
				bound.setSize(x_size[i],y_size[i]);
				if(bound.contains(new Point(x,y))){
					chx = x;
					chy = y;
					showCheck = true;
					if(!allItemsFound[i]) {
						listwords[i] += " CHECK!";
						totalFound++;
						allItemsFound[i] = true;
					}
				}
			}
		}
	}
	
	public void updateText(){
		for(int i = 0; i < listwords.length; i++){
			Window.theList[i] = listwords[i];
		}
		ListPanel.updateList(listwords.length);
	}
	
	public void completionCheck(){
		int count = 0;
		for(int i = 0; i < allItemsFound.length; i++){
			if(allItemsFound[i]){
				count++;
			}
		}
		if(count == 9) {
			System.out.println("Going to end game");
			endGame();
		}
	}
	/**
	 *  public void updateTime()
	 *  Updates the string being printed out by the "watch"
	 */
	public void updateTime(){
			String tens;
			String mins;
			
			// Update the minutes and hours
			tend = (tend + 1) % 60;
			if(tend == 0){
				tstart = (tstart + 1) % 12;
			}
			if(tend == 0){
				// Turn minutes and hours into strings
				if(tend < 10 && tend >= 0){
					mins = "0" + Integer.toString(tend);
				}
				else mins = Integer.toString(tend);
			
				if(tstart == 0)tens = "12";
				else tens = Integer.toString(tstart);
			
				// Make new time string
				time = tens + ":" + mins;
			
			}
			// bg_type is changed to "We're good." in paint once the bgs have been 
			// changed one time
			if(time.equals("5:00")) 
				if(!bg_type.equals("We're good."))bg_type = "SUNSET";
			if(time.equals("6:00")) 
				if(!bg_type.equals("We're better."))bg_type = "NIGHT";
			
			if(time.equals("9:00")) {
				endGame();
			}
		}
	
	/**
	 * Performs actions to end the game and move to the next panel
	 */
	public void endGame(){
		bg_type = "GAMEOVER";
		
		// Set the numbers for the next panel and update it
		VictoryPanel.numFound = totalFound;
		VictoryPanel.possFound = 6;
		VictoryPanel.updatePanel();
		if(totalFound == VictoryPanel.possFound) VictoryPanel.displayImage = 1;
		
		// Update the list
		for(int i = 0; i < Window.theList.length; i++){
			Window.theList[i] = "";
		}
		ListPanel.updateList(listwords.length);
		
		//Change to next panel and reset this level
		cards.show(pane, "Victory");
		System.out.println("Going to reset level");
		resetLevel();
		
	}
	
	private class MenuListener implements ActionListener {

		public void actionPerformed(ActionEvent arg0) {
			if(!isPaused){
				updateTime();
				repaint();
			}
		}
	}
}