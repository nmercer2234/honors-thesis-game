import java.awt.CardLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

public class VictoryPanel extends JPanel{
	ActionListener a = this.new MenuListener();
	static CardLayout cards;
	AllPanels pane;
	static int numFound = 0;
	static int possFound = 0;
	static JLabel goodjob;
	Image l1v, l2v, l3v, l4v, l5v;
	static int displayImage = 0;
	private Timer t = new Timer(250, a);
	
	public VictoryPanel(AllPanels p, CardLayout c){
		pane = p;
		cards = c;
		
		try {
			l1v = ImageIO.read(new File("L1Victory.png"));
			l2v = ImageIO.read(new File("L2Victory.png"));
			l3v = ImageIO.read(new File("L3Victory.png"));
			l4v = ImageIO.read(new File("L4Victory.png"));
			l5v = ImageIO.read(new File("L5Victory.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		JButton back = new JButton("Back to Level Selection");
		goodjob = new JLabel("You found " + numFound + " of the " 
				+ possFound + " hidden objects!");
		goodjob.setFont(new Font("Aharoni", Font.PLAIN, 28));
		
		setLayout(null);

		add(back);
		add(goodjob);

		back.setBounds(10, 500, 200, 50);
		goodjob.setBounds(125, 50, 2000, 100);
		
		back.addActionListener(a);
		
		t.start();
	}
	public static void updatePanel(){	
		if(numFound != possFound) goodjob.setText("You found " + numFound + " of the " 
					+ possFound + " hidden objects!");
		else goodjob.setText("You found all of the hidden objects!");
	}

	private class MenuListener implements ActionListener {

		
		public void actionPerformed(ActionEvent arg0) {	
			repaint();
		
			try{
				if(arg0.getActionCommand().equals("Back to Level Selection")){
					cards.show(pane, "Choose");
				}
			}
			catch(NullPointerException n){}
		}
		
	}
	public void paint(Graphics g){
		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;
		
		if(displayImage == 1) g2.drawImage(l1v, 0, 0, null);
		if(displayImage == 2) g2.drawImage(l2v, 0, 0, null);
		if(displayImage == 3) g2.drawImage(l3v, 0, 0, null);
		if(displayImage == 4) g2.drawImage(l4v, 0, 0, null);
		if(displayImage == 5) g2.drawImage(l5v, 0, 0, null);
	}
}

