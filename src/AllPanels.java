import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class AllPanels extends JPanel {

	private static CardLayout c;
	
	private StartPanel startp;
	private MenuPanel menup;
	private CreditsPanel credp;
	private QuitPanel quitp;
	private ChooseLevelPanel choosep;
	private GameLevel1Panel levonep;
	private VictoryPanel vicp;
	private GameLevel2Panel levtwop;
	private GameLevel3Panel lev3p;
	private GameLevel4Panel lev4p;
	private GameLevel5Panel lev5p;
	private ChooseLevelPanel2 choose2p;
	private ChooseLevelPanel3 choose3p;
	
	public AllPanels(CardLayout cards) throws IOException{
		super(cards);
		c = cards;
		
		startp = new StartPanel(this, c);
		menup = new MenuPanel(this, c);
		credp = new CreditsPanel(this, c);
		quitp = new QuitPanel(this, c);
		choosep = new ChooseLevelPanel(this, c);
		levonep = new GameLevel1Panel(this, c);
		vicp = new VictoryPanel(this, c);
		levtwop = new GameLevel2Panel(this, c);
		lev3p = new GameLevel3Panel(this, c);
		lev4p = new GameLevel4Panel(this, c);
		lev5p = new GameLevel5Panel(this, c);
		choose2p = new ChooseLevelPanel2(this, c);
		choose3p = new ChooseLevelPanel3(this, c);

		
		//Add all of the panels to the CardLayout
		add(startp, "Start");
		add(menup, "Menu");
		add(credp, "Credits");
		add(quitp, "Quit");
		add(choosep, "Choose");
		add(levonep, "Level1");
		add(vicp, "Victory");
		add(levtwop, "Level2");
		add(lev3p, "Level3");
		add(lev4p, "Level4");
		add(lev5p, "Level5");
		add(choose2p, "Choose2");
		add(choose3p, "Choose3");
		
		//Set the First Menu to be visible
		c.show(this,  "Start");
		
	}
}