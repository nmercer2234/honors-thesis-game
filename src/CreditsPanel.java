import java.awt.CardLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;



public class CreditsPanel extends JPanel{
	ActionListener a = this.new CredListener();
	static CardLayout cards;
	AllPanels pane;
	
	public CreditsPanel(AllPanels p, CardLayout c){
		pane = p;
		cards = c;
		
		JButton back = new JButton("Back to Menu");
		JLabel credits = new JLabel("Credits");
		JLabel c1 =  new JLabel("by Nicole Mercer");
		
		setLayout(null);
	
		add(back);
		add(credits);
		add(c1);
		
		back.setBounds(10, 500, 150, 50);
		credits.setBounds(350, 50, 200, 100);
		c1.setBounds(300, 150, 400, 200);
		
		credits.setFont(new Font("Aharoni", Font.PLAIN, 32));
		c1.setFont(new Font("Aharoni", Font.PLAIN, 28));
		
		back.addActionListener(a);
	}

	private class CredListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if(arg0.getActionCommand().equals("Back to Menu")){
				cards.show(pane, "Start");
			}
		}
		
	}
}
