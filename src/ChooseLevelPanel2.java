import java.awt.CardLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JPanel;




public class ChooseLevelPanel2 extends JPanel{
	ActionListener a = this.new MenuListener();
	static CardLayout cards;
	AllPanels pane;
	private Image bgmini, bgmini2;
	
	public ChooseLevelPanel2(AllPanels p, CardLayout c){
		pane = p;
		cards = c;
	
		JButton start = new JButton("Level 3");
		JButton back = new JButton("Back to Menu");
		
		JButton start2 = new JButton("Level 4");
		JButton nextPage = new JButton("Next Page");
		
		setLayout(null);

		add(start);
		add(back);
		add(start2);
		add(nextPage);

		start.setBounds(450, 100, 200, 100);
		back.setBounds(10, 550, 150, 50);
		start2.setBounds(450, 350, 200, 100);
		nextPage.setBounds(625, 550, 150, 50);
		
		start.addActionListener(a);
		back.addActionListener(a);
		start2.addActionListener(a);
		nextPage.addActionListener(a);
		
		try {
			bgmini = ImageIO.read(new File("level3small.png"));
			bgmini2 = ImageIO.read(new File("level4small.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	private class MenuListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(arg0.getActionCommand().equals("Level 3")){
				// Sets and updates the contents of the on-screen list to match
				// the level selected
				String[] list = {  "chipmunk", "", "squirrel", "", "crane", "opossum" ,
						"frog", "raccoon", "duck" };
				for(int i = 0; i < list.length; i++){
				Window.theList[i] = list[i];
				}
				ListPanel.updateList(9);
				cards.show(pane, "Level3");
			}
			if(arg0.getActionCommand().equals("Back to Menu")){
				cards.show(pane, "Start");
			}
			if(arg0.getActionCommand().equals("Level 4")){
				String[] list = { "goose", "", "owl", "cow", "crow", "bat" , "butterfly", 
						"", "bee" };
				for(int i = 0; i < list.length; i++){
				Window.theList[i] = list[i];
				}
				ListPanel.updateList(9);
				cards.show(pane, "Level4");
			}
			if(arg0.getActionCommand().equals("Next Page")){
				cards.show(pane, "Choose3"); //chooselevelpanel3
			}
		}
		
	}
	// Draws the small image of the level
	public void paint(Graphics g){
		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;
		
		g2.drawImage(bgmini, 50, 25, null);
		g2.drawImage(bgmini2, 50, 275, null);
	}
}
