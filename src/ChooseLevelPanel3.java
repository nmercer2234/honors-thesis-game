import java.awt.CardLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JPanel;




public class ChooseLevelPanel3 extends JPanel{
	ActionListener a = this.new MenuListener();
	static CardLayout cards;
	AllPanels pane;
	private Image bgmini;
	
	public ChooseLevelPanel3(AllPanels p, CardLayout c){
		pane = p;
		cards = c;
		
		JButton start = new JButton("Level 5");
		JButton back = new JButton("Back to Menu");
		
		setLayout(null);

		add(start);
		add(back);

		start.setBounds(450, 100, 200, 100);
		back.setBounds(10, 550, 150, 50);
		
		start.addActionListener(a);
		back.addActionListener(a);
		
		try {
			bgmini = ImageIO.read(new File("level5small.png"));

		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	private class MenuListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(arg0.getActionCommand().equals("Level 5")){
				// Sets and updates the contents of the on-screen list to match
				// the level selected
				String[] list = {  "", "deer", "", "turtle", "salmon", "crane" , "cat", 
						"hummingbird", "", "coyote" };
				for(int i = 0; i < list.length; i++){
				Window.theList[i] = list[i];
				}
				ListPanel.updateList(10);
				cards.show(pane, "Level5");
			}
			if(arg0.getActionCommand().equals("Back to Menu")){
				cards.show(pane, "Start");
			}
		}
		
	}
	// Draws the small image of the level
	public void paint(Graphics g){
		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;
		
		g2.drawImage(bgmini, 50, 25, null);
	}
}
