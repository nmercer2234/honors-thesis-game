import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.io.IOException;

import javax.swing.JFrame;


public class Window extends JFrame{

	private static CardLayout c = new CardLayout();
	public static String[] theList = new String[20];
	private AllPanels pane = new AllPanels(c);
	private ListPanel list = new ListPanel();
	
	public Window() throws IOException{
		super("Scavenger Hunt");
		
		// Top part containing menus and game
		add(pane);
		// Bottom part, containing list
		add(list, BorderLayout.PAGE_END);
		
		setSize(800,700);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		revalidate();

	}
}