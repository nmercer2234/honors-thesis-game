import java.awt.CardLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JPanel;




public class ChooseLevelPanel extends JPanel{
	ActionListener a = this.new MenuListener();
	static CardLayout cards;
	AllPanels pane;
	private Image bgmini, bgmini2;
	
	public ChooseLevelPanel(AllPanels p, CardLayout c){
		pane = p;
		cards = c;
		
		JButton start = new JButton("Level 1");
		JButton back = new JButton("Back to Menu");
		
		JButton start2 = new JButton("Level 2");
		JButton nextPage = new JButton("Next Page");
		
		setLayout(null);

		add(start);
		add(back);
		add(start2);
		add(nextPage);

		start.setBounds(450, 100, 200, 100);
		back.setBounds(10, 550, 150, 50);
		start2.setBounds(450, 350, 200, 100);
		nextPage.setBounds(625, 550, 150, 50);
		
		start.addActionListener(a);
		back.addActionListener(a);
		start2.addActionListener(a);
		nextPage.addActionListener(a);
		
		try {
			bgmini = ImageIO.read(new File("bgtest1.png"));
			bgmini2 = ImageIO.read(new File("level2small.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	private class MenuListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(arg0.getActionCommand().equals("Level 1")){
				// Sets and updates the contents of the on-screen list to match
				// the level selected
				String[] list = { "", "cardinal", "squirrel", "butterfly", "duck", 
						"owl", "", "rabbit" };
				for(int i = 0; i < list.length; i++){
				Window.theList[i] = list[i];
				}
				ListPanel.updateList(8);
				cards.show(pane, "Level1");
			}
			if(arg0.getActionCommand().equals("Back to Menu")){
				cards.show(pane, "Start");
			}
			if(arg0.getActionCommand().equals("Level 2")){
				String[] list = { "", "blue jay", "", "", "", "" , "fox", 
						"robin", "deer", "rabbit", "skunk", "bat"};
				for(int i = 0; i < list.length; i++){
				Window.theList[i] = list[i];
				}
				ListPanel.updateList(12);
				cards.show(pane, "Level2");
			}
			if(arg0.getActionCommand().equals("Next Page")){
				cards.show(pane, "Choose2"); //chooselevelpanel2
				System.out.println("NEXT LEVELLL");
			}
		}
		
	}
	// Draws the small image of the level
	public void paint(Graphics g){
		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;
		
		g2.drawImage(bgmini, 50, 25, null);
		g2.drawImage(bgmini2, 50, 275, null);
	}
}
