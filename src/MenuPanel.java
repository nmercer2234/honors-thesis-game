import java.awt.CardLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;



public class MenuPanel extends JPanel{
	ActionListener a = this.new MenuListener();
	static CardLayout cards;
	AllPanels pane;
	
	public MenuPanel(AllPanels p, CardLayout c){
		pane = p;
		cards = c;
		
		JButton start = new JButton("Choose a Level");
		JButton records = new JButton("View Records");
		JButton badges = new JButton("View Badges");
		JButton back = new JButton("Back to Menu");
		
		setLayout(null);

		add(start);
		//add(records);
		//add(badges);
		add(back);

		start.setBounds(300, 200, 200, 100);
		records.setBounds(300, 300, 200, 100);
		badges.setBounds(300, 400, 200, 100);
		back.setBounds(10, 500, 150, 50);
		
		start.addActionListener(a);
		records.addActionListener(a);
		badges.addActionListener(a);
		back.addActionListener(a);
	}

	private class MenuListener implements ActionListener {

		public void actionPerformed(ActionEvent arg0) {
			if(arg0.getActionCommand().equals("Choose a Level")){
				cards.show(pane, "Choose");
			}
			if(arg0.getActionCommand().equals("View Records")){
				//cards.show(pane, "Records");
			}
			if(arg0.getActionCommand().equals("View Badges")){
				//cards.show(pane, "Badges");	
			}
			if(arg0.getActionCommand().equals("Back to Menu")){
				cards.show(pane, "Start");
			}
		}
		
	}
}
