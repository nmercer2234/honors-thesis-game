import java.awt.CardLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;



public class QuitPanel extends JPanel{
	ActionListener a = this.new QuitListener();
	static CardLayout cards;
	AllPanels pane;
	
	public QuitPanel(AllPanels p, CardLayout c){
		pane = p;
		cards = c;
		
		JButton yes = new JButton("Yes");
		JButton no = new JButton("No");
		JLabel question = new JLabel("Do you want to quit?");
		
		setLayout(null);
		
		add(yes);
		add(no);
		add(question);
		
		yes.setBounds(250, 300, 150, 50);
		no.setBounds(400, 300, 150, 50);
		question.setBounds(300,150,400,200);
		
		question.setFont(new Font("Aharoni", Font.PLAIN, 18));
		
		yes.addActionListener(a);
		no.addActionListener(a);

	}

	private class QuitListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			// For Quit Panel:
			if(arg0.getActionCommand().equals("Yes")){
				System.exit(0);
			}
			if(arg0.getActionCommand().equals("No")){
				cards.show(pane, "Start");
			}
		}

		
	}
}
