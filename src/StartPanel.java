import java.awt.CardLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;



public class StartPanel extends JPanel{
	ActionListener a = this.new StartListener();
	static CardLayout cards;
	AllPanels pane;
	
	public StartPanel(AllPanels p, CardLayout c){
		pane = p;
		cards = c;
		
		JButton start = new JButton("Start Game");
		JButton credits = new JButton("View Credits");
		JButton quit = new JButton("Quit");
		JLabel title = new JLabel("Scavenger Hunt");
		
		setLayout(null);
		
		title.setFont(new Font("Aharoni", Font.PLAIN, 32));
		
		add(title);
		add(start);
		add(credits);
		add(quit);
		
		title.setBounds(285, 50, 300, 100);
		start.setBounds(300, 200, 200, 100);
		credits.setBounds(300, 300, 200, 100);
		quit.setBounds(300, 400, 200, 100);
		
		start.addActionListener(a);
		credits.addActionListener(a);
		quit.addActionListener(a);
	}

	private class StartListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if(arg0.getActionCommand().equals("Start Game")){
				//makeMenuVisible();
				cards.show(pane, "Menu");
			}
			if(arg0.getActionCommand().equals("View Credits")){
				cards.show(pane, "Credits");
			}
			if(arg0.getActionCommand().equals("Quit")){
				cards.show(pane, "Quit");	
			}
		}
		
	}
}
