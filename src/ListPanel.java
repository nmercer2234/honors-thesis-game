import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


public class ListPanel extends JPanel{
	static JLabel[] txt = new JLabel[20];
	public static ImageIcon checked;
	public static ImageIcon unchecked;
	public ListPanel(){
	
		setSize(100, 200);
		
		checked = new ImageIcon("txtcheck.png", "A checked box");
		unchecked = new ImageIcon("txtbox.png", "An unchecked box");
		
		for(int i = 0; i < 20; i++){
			txt[i] = new JLabel("");
			//if(i != 0 && i != 6) add(txt[i]); this was here for something in level1...
			add(txt[i]);
		}
		
	}
	
	public static int countItems(){
		int i = 0, count = 0;
		while(i < 20){
			if(Window.theList[i] != null) count++;
			i++;
		}
		return count;
	}
	
	public static void updateList(int loop){
		for(int i = 0; i < loop; i++){
			if(!Window.theList[i].equals("")){
				String temp = Window.theList[i].replaceAll("CHECK!", "");
				temp.trim();
				txt[i].setText(temp);
				if(Window.theList[i].contains("CHECK!")){
					txt[i].setIcon(checked);
				}
				else{
					txt[i].setIcon(unchecked);
				}
			}
			else{
				txt[i].setText(null);
				txt[i].setIcon(null);
			}
		}
	}
}
